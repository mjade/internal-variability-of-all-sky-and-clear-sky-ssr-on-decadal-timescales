'''
A piece of code, which calculates the probability of occurence of an unforced SSR trend, based on CMIP6 piControl data.
The code accompanies the following paper:

Chtirkova, B., Folini, D., Correa, L. F., & Wild, M. (2022). Internal variability of all-sky and clear-sky surface solar radiation on decadal timescales. Journal of Geophysical Research: Atmospheres, 127, e2021JD036332. https://doi.org/10.1029/2021JD036332

Copyright (c) 2021, ETH Zurich, Boriana Chtirkova
'''

import pickle
from numpy import sqrt, abs, argmin
from scipy.stats import norm


def ll_to_xy(lat, lon, grid_lat, grid_lon):
    """ll_to_xy.
       Finds the indices in a regular latitude-longitude grid, which are the closest in value to a given lat-lon pair.
    :param float lat:  Latitude of target point in degrees. Takes values from -90 to 90.
    :param float lon:  Longitude of target point in degrees. Takes values from -180 to 180.
    :param grid_lat:   An array, containing the latitude values of the centers of the grid cells.
    :param grid_lon:   An array, containing the latitude values of the centers of the grid cells.
    """
    lat_diff = abs(lat - grid_lat)
    lon_diff = abs(lon - grid_lon)
    idx_lat = argmin(lat_diff)
    idx_lon = argmin(lon_diff)
    return idx_lat, idx_lon


def get_probability(trend_magnitude, trend_period, obs_lon, obs_lat):  # give magnitude per year
    """get_probability.
       Calculates the probability of occurence of a trend based on the standard deviation of the underlying time series.
       Sigma_ts and propablity calculations for both all-sky and clear-sky SSR are printed to standard output.
    :param float trend_magnitude:  The magnitude of the trend in Wm-2/year (usually, the slope of the linear fit is taken).
    :param float trend_period:     The length of the trend in years.
    :param obs_lon:                The longitude of the location, at which we check.
    :param obs_lat:                The latitude of the location, at which we check.
    """

    # The pickle file, in which the gridded data is stored.
    pickle_file = 'cmip6_sigma_ts.pickle'
    data_dict = pickle.load(open(pickle_file, 'rb'))
    variable_name_string = ''

    # Take the grid cell centers
    grid_lat, grid_lon = data_dict['lats'], data_dict['lons']

    # Find the indices in the grid corresponding to the observational point.
    # (The same as nearest neighbour remapping.)
    idx_lat, idx_lon = ll_to_xy(lat=obs_lat, lon=obs_lon, grid_lat=grid_lat, grid_lon=grid_lon)
    print('Nearest lat-lon grid point: ', grid_lat[idx_lat], grid_lon[idx_lon])

    # Loop over all-sky and clear-sky SSR
    for variable_name in ['rsds', 'rsdscs']:

        # Take CMIP6 multimodel statistics from pickle file
        sigma_ts = data_dict[variable_name]['cmip6_multimodel_sigma_ts']
        spread_sigma_ts = data_dict[variable_name]['cmip6_intermodel_spread_sigma_ts']
        applicability_region = data_dict[variable_name]['applicability_region']

        # Take statistics at the observational point:
        # Standard deviation of the underlying time series (CMIP6 multi-model median, i.e. 50th pecentile)
        sigma_ts_at_obs = sigma_ts[idx_lat, idx_lon]  # float
        # Inter-model spread of the standard deviation, defined as the difference between
        # the 90th percentile and 10th percentile of all models
        spread_at_obs = spread_sigma_ts[idx_lat, idx_lon]  # float
        # The statistical link at that point can be made if the distribution is gaussian
        # and there's approximately zero autocorrelation in time.
        appllicability_at_obs = applicability_region[idx_lat, idx_lon]  # bool

        if variable_name == 'rsds':
            variable_name_string = 'All-sky SSR'
        elif variable_name == 'rsdscs':
            variable_name_string = 'Clear-sky SSR'

        print(variable_name_string)
        print('CMIP6 mulmimodel median sigma_ts at grid point: ', sigma_ts_at_obs, 'W/m2')
        print('CMIP6 intermodel spread at the same grid point: ', spread_at_obs, 'W/m2')

        if appllicability_at_obs == 0:
            print('Grid point falls within applicable region.')
        elif appllicability_at_obs == 1:
            print('Grid point falls DOES NOT fall within applicable region.')

        # Calculate probability
        t = trend_magnitude
        N = trend_period
        Z = t / (sqrt(12) * N**(-3 / 2) * sigma_ts_at_obs)
        p = norm.cdf(Z)
        if trend_magnitude > 0:
            probability = 1 - p
        else:
            probability = p

        print('The probability of a ', variable_name_string.lower(), ' trend with a magnitude ', t, ' Wm-2/year over a period of ',
              N, 'years to occur solely due to internal variability at the specified location is ', probability * 100, '%.')

    return


def main():
    print('Stockholm')
    station_lat, station_lon = 59.326242, 17.841923  # Stockholm
    t = 2.5  # in Wm-2/year
    n_y = 4  # number of years
    get_probability(trend_magnitude=t, trend_period=n_y, obs_lon=station_lon, obs_lat=station_lat)

    print('Lindenberg')
    station_lat, station_lon = 52.21, 14.122  # Lindenberg
    t = 0.28  # in Wm-2/year
    n_y = 18  # number of years
    get_probability(trend_magnitude=t, trend_period=n_y, obs_lon=station_lon, obs_lat=station_lat)


if __name__ == "__main__":
    main()
