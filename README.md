# Internal variability of all-sky and clear-sky SSR on decadal timescales

Calculates the probability of occurrence of an unforced trend at a specific location, using the standard deviation of the underlying timeseries at that location. The method is described in [1] and [2].

## Requirements
Python 3.6 or higher, NumPy and SciPy.

## Usage
The script `calculate_probability.py` contains a few functions used to calculate the desired probability. The pickle file `cmip6_sigma_ts.pickle` contains the CMIP6 piControl multi-model standard deviation of all-sky (rsds) and clear-sky (rsdscs) SSR, interpolated to a 0.5 degree grid. The user should input the geographical coordinates, trend magnitude and trend length and the script will output the probability of such a trend to be entirely due to internal variability. An example for Stockholm and Lindenberg is given (see lines 96-106 in `calculate_probability.py`).

[1] Folini, D., Dallafior, T. N., Hakuba, M. Z., and Wild, M. (2017), Trends of surface solar radiation in unforced CMIP5 simulations, J. Geophys. Res. Atmos., 122, 469– 484, doi:10.1002/2016JD025869. 

[2]  Chtirkova, B., Folini, D., Correa, L. F., & Wild, M. (2022). Internal variability of all-sky and clear-sky surface solar radiation on decadal timescales. Journal of Geophysical Research: Atmospheres, 127, e2021JD036332. https://doi.org/10.1029/2021JD036332 

